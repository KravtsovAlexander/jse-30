<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.t1.kravtsov.tm</groupId>
    <artifactId>task-manager-server</artifactId>
    <version>1.30.0</version>
    <packaging>jar</packaging>

    <name>Task Manager Server</name>
    <url>https://this-site-does-not-exist.com/</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <scm>
        <connection>scm:git:http://none</connection>
        <developerConnection>scm:git:http://none</developerConnection>
        <url>scm:git:http://none</url>
    </scm>

    <developers>
        <developer>
            <id>kravtsov</id>
            <name>Alexander Kravtsov</name>
            <email>aekravtsov@nota.tech</email>
        </developer>
    </developers>

    <dependencies>
        <dependency>
            <groupId>ru.t1.kravtsov.tm</groupId>
            <artifactId>task-manager-domain</artifactId>
            <version>1.29.0</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.12.1</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <version>2.12.1</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-xml</artifactId>
            <version>2.12.1</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.moxy</artifactId>
            <version>2.7.2</version>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.3.1</version>
        </dependency>
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <version>0.9.11</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>1.2.6</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.6</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.30</version>
        </dependency>
        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-manifests</artifactId>
            <version>1.1</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>22.0.0</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>task-manager</finalName>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>true</doCheck>
                    <doUpdate>false</doUpdate>
                    <revisionOnScmFailure>true</revisionOnScmFailure>
                    <format>${version}.{0,number}</format>
                    <items>
                        <item>buildNumber</item>
                        <item>timestamp</item>
                    </items>
                </configuration>
            </plugin>
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>4.9.10</version>
                <executions>
                    <execution>
                        <id>get-the-git-infos</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>validate-the-git-infos</id>
                        <goals>
                            <goal>validateRevision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>../lib</classpathPrefix>
                            <mainClass>ru.t1.kravtsov.tm.Application</mainClass>
                        </manifest>
                        <manifestEntries>
                            <goupId>${groupId}</goupId>
                            <artifactId>${artifactId}</artifactId>
                            <version>${version}</version>
                            <buildNumber>${buildNumber}</buildNumber>
                            <timestamp>${timestamp}</timestamp>

                            <developer>Alexander Kravtsov</developer>
                            <email>aekravtsov@nota.tech</email>

                            <!--suppress UnresolvedMavenProperty -->
                            <buildHash>${git.commit.id.abbrev}</buildHash>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitBranch>${git.branch}</gitBranch>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitId>${git.commit.id}</gitCommitId>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitTime>${git.commit.time}</gitCommitTime>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitMessage>${git.commit.message.full}</gitCommitMessage>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitterName>${git.commit.user.name}</gitCommitterName>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitterEmail>${git.commit.user.email}</gitCommitterEmail>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <descriptor>src/main/assembly/zip.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
