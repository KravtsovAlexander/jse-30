package ru.t1.kravtsov.tm.repository;

import lombok.NoArgsConstructor;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.model.Session;

@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
