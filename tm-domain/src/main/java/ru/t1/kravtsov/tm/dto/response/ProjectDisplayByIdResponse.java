package ru.t1.kravtsov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Project;

@NoArgsConstructor
public class ProjectDisplayByIdResponse extends AbstractProjectResponse {

    public ProjectDisplayByIdResponse(final @Nullable Project project) {
        super(project);
    }

}
