package ru.t1.kravtsov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByNameRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    public ProjectRemoveByNameRequest(final @Nullable String token) {
        super(token);
    }

}
